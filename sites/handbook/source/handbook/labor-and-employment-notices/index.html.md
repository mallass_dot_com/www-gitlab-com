---
layout: handbook-page-toc
title: "Labor and Employment Notices"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Since GitLab is an [all remote](/company/culture/all-remote/) company we don't have a physical worksite or breakroom wall upon which to post important labor and employment notices, so this page is our version of that.

## US Employment Posters:

## City of San Francisco

* San Francisco Minimum Wage increases to $15.59 per hour [(June 2019)](ttps://gitlab.com/gitlab-com/people-ops/Compensation/uploads/e261b0f24e1b2d5bf382366bb400cf53/sf-minimum-wage-2019-06.pdf).

## EEOC (U.S. Equal Employment Opportunity Commission) Notices

Information related to the [EEO is the Law poster](https://www1.eeoc.gov/employers/poster.cfm).

* ["EEO is the Law" English poster for screen readers.](https://www.eeoc.gov/sites/default/files/migrated_files/employers/poster_screen_reader_optimized.pdf)
* ["EEO is the Law" English poster for printing.](https://www.eeoc.gov/sites/default/files/migrated_files/employers/eeoc_self_print_poster.pdf)
* ["EEO is the Law" Spanish poster for printing.](https://www.eeoc.gov/sites/default/files/migrated_files/employers/eeoc_self_print_poster_spanish.pdf)


## E-Verify 

[Notice of E-Verify Participation Poster](https://www.e-verify.gov/sites/default/files/everify/posters/EVerifyParticipationPoster.pdf) 
[Right to Work Poster](https://www.e-verify.gov/sites/default/files/everify/posters/IER_RighttoWorkPoster.pdf)

## Fair Labor Standards Act (FLSA) Minimum Wage Poster

[Fair Labor Standards Act Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/minwagep.pdf)

## Family and Medical Leave Act 

[Family and Medical Leave Act (FMLA) Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/fmlaen.pdf)

## OSHA Job Safety and Health

[Job Safety and Health: Its the Law Poster](https://www.osha.gov/Publications/osha3165-8514.pdf)

## Germany Employment Posters:

[Working Hours Act](https://www.gesetze-im-internet.de/arbzg/index.html) 

## Canada Employment Posters:

### Ontario
[Occupational Health & Safety Poster](https://files.ontario.ca/mltsd_2/mltsd-prevention-poster-en-2020-07-22.pdf)

[Occupational Health & Safety Act](https://www.ontario.ca/laws/statute/90o01)

### British Columbia
[Working in B.C.](https://www2.gov.bc.ca/assets/gov/employment-business-and-economic-development/employment-standards-workplace-safety/employment-standards/factsheets-pdfs/working_in_bc_infosheet.pdf)

[Occupational Health and Safety Regulation](https://www.worksafebc.com/en/law-policy/occupational-health-safety/searchable-ohs-regulation/ohs-regulation)

[Workers Compensation Act](https://www.worksafebc.com/en/law-policy/occupational-health-safety/searchable-ohs-regulation/ohs-guidelines/guidelines-for-workers-compensation-act)

### Alberta:
[Workers Compensation Act](https://www.wcb.ab.ca/assets/pdfs/employers/123_english.pdf)

[Employment Standards Code](https://www.alberta.ca/assets/documents/es-general-online-poster.pdf)

### Manitoba: 
[Safe Work Manitoba](https://www.safemanitoba.com/Page%20Related%20Documents/resources/BR_EveryonesResponsibilityLong_15SWMB.pdf)

## Europe

[European Agency for Safety and Health at Work](https://osha.europa.eu/en)

### Netherlands
[European Agency for Safety and Health at Work - Netherlands](https://osha.europa.eu/en/about-eu-osha/national-focal-points/netherlands)
[OSH - Netherlands](https://www.arboineuropa.nl/en/arbo-in-the-netherlands/)
[Health and Safety at Work - Netherlands](https://business.gov.nl/regulation/health-safety-work/)
