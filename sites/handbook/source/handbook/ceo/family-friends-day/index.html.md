---
layout: handbook-page-toc
title: "Family and Friends Day"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---

At GitLab, we are [family and friends first, work second](/handbook/values/#family-and-friends-first-work-second). Because we want to ensure that people are making their well-being a top priority and that we are living out our values, we will occassionally call a "Family and Friends Day." On this day, we will close the doors to the GitLab virtual office, reschedule all meetings, and have a **publicly visible shutdown**.

Team members can share about their Family and Friends Day in the `#family-and-friends-day` Slack channel after the event, or publicly on social media such as Twitter, LinkedIn, or wherever they're most comfortable using the hashtag #FamilyFriends1st. Sharing is optional. Taking the day off is strongly encouraged if your role allows it.

## Upcoming Family and Friends Days 

1. `TBD`

We are living in unprecedented times and are trying to give our Team Members some extra space in their lives outside of work. In line with our [Paid Time Off](/handbook/paid-time-off/) policy, we encourage GitLab Team Members to continue to take additional days off, as needed. Family and Friends Day is a reminder to do this.

## Past Family and Friends Days 

1. The first Family and Friends Day was `2020-05-01`. 
1. Due to its success in reinforcing our message and supporting team member wellness, the second Family and Friends Day was celebrated on `2020-06-12`. 
1. A third Family and Friends Day was celebrated on `2020-08-14`.
1. A fourth Family and Friends Day was celebrated on `2020-10-09`.


## FAQ about Family and Friends Day

### Who determines upcoming Family and Friends Days?
Any GitLab team member is able to propose a Family and Friends day. To propose a Family and Friends day please follow the steps outlined below:
1. Review the [GitLab Team Meetings calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV82ZWtiazhmZnFua3VzM3FwajlvMjZycWVqZ0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) for major conflicts, review major holidays, and avoid the end of the quarter to ensure our Sales team can participate before determing a date proposal. 
1. Submit a merge request to modify the `Upcoming Family and Friends Days` section of this page with the date of your Family and Friends day proposal. 
1. Post the MR in the `#e-group` Slack channel and `@mention` the `@exec-group` and `@exec-admins` for feedback.
1. If feedback is positive, assign the MR to our CEO. 
1. Post the MR in the `#ceo` Slack channel for CEO review and merging. 

### I'm in a role which requires me to work that day. How can I still benefit from this initiative? 
If you are in a role that requires you to work on Family and Friends Day, you can work with your manager to find an alternative day. We encourage you to consider the following business day as the preferred second choice for a day away, but do what works best for you and your team.

### What if the date is a public holiday or non-working day in my country? How does this apply to me?
We encourage you to take off the next working day. If this day isn't an option, work with your manager to find another day that works for you and your team.

### How is this any different than our vacation policy?
Nothing about our [Paid Time Off](/handbook/paid-time-off/) policy is changing. We wanted to designate a specific day in order to more proactively force a pause for team members. If most of the company isn't working, there is less pressure for you to do so.

### What about client or prospect meetings that conflict? 
If you feel that this meeting can be rescheduled without any setbacks to the business, please go ahead and do so. If you have a meeting that would be hard to reschedule or would jeopardize the business results, please work with your manager to find another day that would work for both you and your team.

### What if I'm out sick on either of those days?
Feel better! Please work with your manager to find another day that works for you and your team.

### How do I communicate that I'm off that day?  
We'll assume that most people are off on Family and Friends Day, but we know that some people will take other days. 

Please update [PTO Ninja](/handbook/paid-time-off/#pto-ninja) in Slack. You can select `Create an OOO Event` and find `Family & Friends Day` in the drop-down menu of `What type of OOO is this?`. 

Feel free to block your calendar with "Family and Friends Day" to share whatever day you take.
