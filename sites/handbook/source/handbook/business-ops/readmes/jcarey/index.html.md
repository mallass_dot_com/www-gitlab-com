---
layout: markdown_page
title: "Jamie Carey's README"
description: "Senior Business Systems Analyst"
---

Hello! This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

- [Jamie's team page bio](/company/team/#j.carey)
- [Jamie's GitLab contribution graph](https://gitlab.com/j.carey)
- [Jamie's LinkedIn profile](https://www.linkedin.com/in/jamiemcarey/)

## About me

- I have a fine arts degree, and most of my working life has been spent in non-tech industries like restaurants, retail, and healthcare.

- I've traveled quite a bit, but I'm enjoying being home more and more.

- I love animals and grew up with way too many pets. I love how remote work gives me more opportunity to spend time with the animals in my life.

- I enjoy connecting people and pulling on threads which is why being a business systems analyst has suited me well. It's a role in which I can be curious and explore ideas, practice writing, and follow information — then connect people to solve problems.

- The most important GitLab [values](/handbook/values/) to me are [Diversity, Inclusion and Belonging](/handbook/values/#diversity-inclusion), [Transparency](/handbook/values/#transparency), and [Collaboration](/handbook/values/#collaboration). We will only achieve our goals by choosing to bring in all perspectives, listening, being open, and working together.

## How you can help me

- Bring me into a conversation, meeting (feel free to mark optional!), or issue earlier than later. It helps me to connect dots when I know what the big picture is. We all have knowledge that someone else needs.

- If you need me or want me to do something other than be aware, please be as specific as possible.

- When you learn something about how something works, please [add it to the handbook](/handbook/handbook-usage/#why-handbook-first)! If you build something, document it in the handbook! Most of my time is spent trying to answer a question I _know_ someone knows.

- Give me a minute to think. I'm that person that needs that awkward silence.

- Write down what you need me to know. Volunteer to take notes in my meetings. I'm more visual than auditory. Make diagrams!

- Explain to me the big picture before you explain the parts.

- Set your meetings to modifiable by all. If you need to adjust a meeting I set up with you, go ahead and move it!

- Add a [detailed agenda](/company/culture/all-remote/meetings/#have-an-agenda) to your meeting invite with links to relevant documents please!

## My working style

- I tend to "listen" for information that may be related to my work. I like to hang out in (too many) Slack channels or peruse issues that may provide new or related information that I can link to a larger picture or problem.

- As a perfectionist, I'm constantly learning to let go of the 20% that can stand in the way of accomplishing something. In tech, it's easy to chase edge cases and create complicated workflows that might catch every use case, but at what cost to the project at hand and the projects to come?

- I set my meetings to end early, and I recommend you do as well. As we are all remote, there's no next meeting room, so there's less walking, and less quick trips to be human between meetings. Give yourself and everyone a break and get into the habit of setting your Google calendar to "[speedy meetings](/handbook/communication/#scheduling-meetings)."

- I like to think about what you are saying before I come up with a plan or solution.

- I like when people label things, remind me of their names or titles, or explain what they do or mean.

- I prefer writing things out over acronyms (at least at the beginning of a document or issue). You can't guarantee that everyone knows what all the acronyms mean, and different departments may use the same acronym, but it may have different meanings.

- If you want to meet with me, just throw something on my calendar.

## What I assume about others

- I assume you know what your system/tool does or you can find out.

- I assume you also want to solve the problems I do. I know in reality that's not necessarily true, so if you don't have time for my questions or work, it's better to just let me know or I'll likely keep coming back.

- I assume you have a real life beyond work — people, hobbies, pets, kids, doctor's visits. You never have to apologize for being human to me.

- I assume that you enjoy what you do for work. If you don't, and you want to explore the possibilities of how to take what you _do_ like about what you do and figure out how to do more of that, let me know! I love brainstorming.

- If you are in Product or Engineering, you know what you are doing in GitLab. If you aren't, I assume you may need help or need some assistance with issues and merge requests, _especially_ when you are first coming onboard.

## What I want to earn

I want to be a trusted advisor and partner in the business. I enjoy improving processes, operations, integrations, relationships, _whatever_ it is that makes things complicated. I want to be on a team that you and your team wants to invite into conversations when you want to start fixing something. I want you to come to us and outline your business problem _before_ you have a solution proposal because you know we will walk alongside you to explore all the options and risks and help you figure out the best way forward.

## Communicating with me

Mentioning me in issues and merge requests are probably the best way to bring me into something since they are able to be left open in tabs. (I have too many tabs open.) But really, Slack me, email me, Zoom me, set up a meeting — whatever works best for you.

## Focus Areas

- [Portal Ecosystem](/handbook/business-ops/business_systems/portal/)
- Supporting Go to Market Operations
- Supporting Customer Success Operations
- [Business System Analyst Team](/handbook/business-ops/business_systems/)

Return to the main [Business Operations handbook section.](/handbook/business-ops/)
    
