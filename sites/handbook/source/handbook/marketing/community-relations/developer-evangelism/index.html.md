---
layout: handbook-page-toc
title: "Developer Evangelism"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goal

To build GitLab's technical brand with deep, meaningful conversations on engineering topics relevant to our industry by leveraging our community of team-members and the broader ecosystem.

### What do we do?

The GitLab developer evangelists are engineers who enjoy learning and teaching the latest technology topics with the wider community. The evangelists are supported by program management within the team and also collaborate closely with the content and PR teams in corporate marketing to amplify their voices. There are two specific goals for the team:

1. **Thought leadership:** Drive GitLab awareness and brand value as full-time Developer Evangelists participating in thought leadership on au courant topics in the cloud native and cloud computing ecosystem.
1. **Ecosystem engagement:** Establish GitLab's technical thought leadership by building GitLab's influence in the tech community through participation in open source projects, foundations, and other consortiums. A description of consortiums can be found in our [Open Source Program handbook page](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships).
1. **Marketing value:** Work with other teams in GitLab (such as content, social, marketing programs) to repurpose, maximize the value of, and measure the impact of content created for thought leadership and ecosystem engagement.

### Developer Advocacy / Relations / Evangelism Insights

A good overview with specific area definitions can be found in the [DevRel Notebook](https://github.com/konradsopala/devrel-notebook). 

We engage with Developer advocacy, relations and evangelism friends on social media:

- [Twitter list: Dev Avocados](https://twitter.com/i/lists/1012393598262874112/members) by Quintessence Anx (DevRel Collective founder)
- [Twitter list: DevRel](https://twitter.com/i/lists/1288789359865606145/members) by Michael
- [DevRel Contacts](https://docs.google.com/document/d/1ZX4BIwJTL0nVdkpRvLYDdk67jQfkRD_ErJWWHn-4KP8/edit) (Internal)

Our KPIs and processes follow industry best practices. We regularly iterate on new ideas and different strategies. The following articles can be helpful to explore new ways of Developer Evangelism:

- [Measuring Success and KPIs in Developer Relations - Community Contributed Outline](https://dev.to/tessamero/measuring-success-and-kpis-in-developer-relations-community-contributed-outline-1383)
- [Measuring the Impact of Your Developer Relations Team](https://openviewpartners.com/blog/measuring-the-impact-of-your-developer-relations-team/)
- [Developer evangelism and GitHub metrics - Or why stars are not the answer](https://devrel.net/strategy-and-metrics/developer-evangelism-github-metrics)
- [Developer Evangelism with Tessa Mero of Cisco](https://openchannel.io/blog/developer-evangelism-tessa-mero-cisco/)
- [What Do You Do as a Developer Advocate (🥑) at Elastic?](https://xeraa.net/blog/2020_what-do-you-do-as-a-developer-advocate-at-elastic/)


## Who we are

We are members of the [Community Relations team](/handbook/marketing/community-relations/).

### Team members and focus areas

1. [Abubakar Siddiq Ango](/company/team/#abuango) - Developer Evangelism Program Manager
    - DevSecOps with a focus on the Cloud Native Ecosystem
        - Kubernetes
        - CI/CD
    - Program management
        - Running the CFP process for conferences
        - Organizing Developer evangelism team's content creation and repurposing efforts
    - Language skills: English, Yoruba, Hausa
1. [Brendan O'Leary](/company/team/#brendan) - Senior Developer Evangelist
    - DevOps with a focus on the application developer perspective
        - SCM
        - GitOps
        - CI
        - .NET and Javascript communities
    - Language skills: English
1. [Michael Friedrich](/company/team/#dnsmichi) - Developer Evangelist
    - DevOps with a focus on the SRE, Ops engineers' perspective
        - CI/CD
        - Serverless
        - Observability
    - Language skills: English, German, Austrian
1. [John Coghlan](/company/team/#john-coghlan) - Manager, Developer Evangelism
    - Developer Evangelism
    - Meetups
    - Heroes

## How we work

### Team Bookmarks

- CFP
    - [Create CFP Meta Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta)
    - [Create CFP Submission Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
- [Create Developer Evangelist Request Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request)
- [Events Spreasheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit#gid=0)
- [Events Calendar](#our-events-list)
- [Team Shared Drive](https://drive.google.com/drive/u/0/folders/0AEUOlCStMBC9Uk9PVA) (Internal)
- [Weekly Meeting Agenda](https://docs.google.com/document/d/1oPXtlWNDbeut-dbFDLXBCfiBJLWwbzjjK9CFu5o8QzU/edit#) (Internal)
- [Team Collab Session Agenda Doc](https://docs.google.com/document/d/1Hs2DrUf9QJQR8fSsNO9WgX7c9sS46NSjEDuKHrrvPv0/edit#heading=h.vgcz1npa9iy) (Internal)
- Team Issue Boards
    - [General](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism)
    - [CFP](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name%5B%5D=DE-CFP)
    - [Content](https://gitlab.com/groups/gitlab-com/-/boards/1624080?&label_name%5B%5D=dev-evangelism)
    - [Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1672643?label_name%5B%5D=dev-evangelism)

### Social Media

We build our thought leadership on social media. See [Developer Evangelism on Social Media](/handbook/marketing/community-relations/developer-evangelism/social-media/) to learn more about our strategies and become an evangelist yourself.

### Projects

Our team maintains many projects to help show off technical concepts, engage with communities, provide examples of using GitLab with other technologies, and automate our team processes. See [Developer Evangelism Projects](/handbook/marketing/community-relations/developer-evangelism/projects/) for a list of all of those projects.

### CFPs

The Developer Evangelism team has several interactions with "CFPs" - calls for proposals for speaking at conferences. Firstly, many of our Developer Evangelists directly contribute by speaking [at conferences themselves](#request-a-developer-evangelist). In addition, our program team [manages responses to CFPs](/handbook/marketing/community-relations/developer-evangelism/cfps/) for GitLab through our issue boards.

### Issue Tracker

We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). We own the label `dev-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups.

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism).

### Issue Labels

Using the [dev-evangelism](https://gitlab.com/groups/gitlab-com/-/labels?search=dev-evangelism) label on an issue means we are working on it or participating in the ongoing conversation. Team members are subscribed to issue/MR updates for this label.

The `dev-evangelism` label is accompanied by some labels we use to organize our work.

<i class="fas fa-info-circle" style="color: rgb(49, 112, 143)
;"></i>You only need to use the `dev-evangelism` label on issues requiring the attention of the Developer Evangelism team. Other labels are applied by the team as their state change or as contained in various issue templates.

{: .alert .alert-info}

#### General Labels

| **CFP Labels** | **Description** |
| ---------- | ----------- |
| `DE-DueSoon` | This is used to monitor DE issues that are due soon |
| `DE-Peer-Review` | Feedback is needed on the issue from DE team members |
| `DE-Ops` | Used to label issues related to the Developer Evangelism `Ops in DevOps` theme |
| `DE-Dev` | Used to label issues related to the Developer Evangelism `Dev in DevOps` theme |
| `DE-k8s` | Used to label issues related to the Developer Evangelism `Kubernetes` theme |

#### Developer Evangelism Team Administrative Processes

The team creates issues for iteration, team discussions, and other issues for internal processes. These issues are tracked using the following labels:

| **Process Labels** | **Description** |
| -------------- | ----------- |
| `DE-Process::Open` | Process related issues that are still being discussed or worked |
| `DE-Process::Pending` | Process related issues on hold due to an external factor |
| `DE-Process::Done` | Completed Process issues |
| `DE-Process::FYI` | Issues that require no action from the team, but need to be aware of |

### Request a Developer Evangelist

If you require a Developer Evangelist's help for anything, please use this ["Developer Evangelist Request" issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request) to create an issue, and we will get back to as soon as possible. On creating the request issue, two labels are applied: `DE-Request::New` & `DE-Request-Type::External` and transitioned through different states as listed below:

| **Request Labels** | **Description** |
| -------------- | ----------- |
| `DE-Request::New` | Newly created requests |
| `DE-Request::Open` | Requests that are still being worked on or considered |
| `DE-Request::Done` | Completed requests |
| `DE-Request::Cancelled` | Requests not considered by the team |
| `DE-Request-Type::External` | Identifies Requests created by other teams |
| `DE-Request-Type::internal` | Identifies Requests created by Developer Evangelism team members |

#### CFPs (Call for Proposals)

See [see Requesting a Developer Evangelist to submit a CFP](/handbook/marketing/community-relations/developer-evangelism/cfps/) to request a Developer Evangelist to submit to a CFP for a corporate, field, or partner event.

#### Content Creation

Covers any content request, Webcast, Interview, Meetup, etc. These content issues first get the parent `dev-evangelism` label applied, and are then transitioned through different stages using the scoped labels below:

| **CFP Labels** | **Description** |
| ---------- | ----------- |
| `DE-Content::new-request` | New content request |
| `DE-Content::draft` | A member of the TE team is working on the content |
| `DE-Content::review` | Content Author is seeking review from other team memebers |
| `DE-Content::published` | Content has been published |
| `DE-Content::Repurposing` | Content is being repurposed on other platforms or mediums |
| `DE-Content::cancelled` | Content is no longer needed, and no one is working on it |

``` plantuml
start
: dev-evangelism, DE-Content::new-request;
if (Content Request Cancelled at any stage) then (yes)
     :dev-evangelism, DE-Content::cancelled;
else
    : dev-evangelism, DE-Content::draft;
    : dev-evangelism, DE-Content::review;
    : dev-evangelism, DE-Content::published;
    : dev-evangelism, DE-Content::Repurposing;
endif

stop
```

### Slack

GitLab team members can also reach us at any time on the [#developer-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.

## Useful links

1. [How to be an evangelist](/handbook/marketing/community-relations/developer-evangelism/how-to-be-an-evangelist/)
1. [How to submit a successful conference proposal](/handbook/marketing/community-relations/developer-evangelism/writing-cfps/)
1. [Consortiums we work with](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships)
1. [Speaking logistics](/handbook/marketing/community-relations/developer-evangelism/speaking-logistics/)
