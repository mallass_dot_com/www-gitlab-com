---
layout: handbook-page-toc
title: Manager Challenge
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Manager Challenge page. We are rolling out a pilot version of a 4 Week Manager Challenge in September 2020, "The GitLab Manager Challenge Program." All details relating to the challenge can be found here on this page. The first iteration of the program will be a pilot to a subset of team members at GitLab. Once the pilot has concluded, we will roll out the program to the broader GitLab team!

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vST_3shd7g0Y6E46JaCdtpXKHfj6D8TAjF-fgZ4IiZ_1NETN2f8ROjBE6NtOpCSs0YXwWgYq-oHryO9/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Manager Challenge Learning Objectives

You may be asking yourself what you will achieve by taking part in the Manager Challenge program. Our goal is that participants walk away with the following skills after completing the four-week challenge: 
- Improve your performance by strengthening your management skills
- Develop a management style that incorporates a whole-person approach to leading others
- Handle difficult management situations with greater certainty in your decisions
- Use emotional intelligence to improve leadership and build more effective relationships
- Build an inclusive environment for your people that is built on trust
- Grow and strengthen skills in leading remote teams at GitLab

### Overview

**What** 

The Four Week Manager Challenge Program is a blended learning approach that incorporates self-paced daily challenges and live learning sessions to build foundational management skills. The program incorporates leadership assessments, interactive live learning sessions, and digital learning. 

**Why** 

The program is intended to build a set of baseline skills that complement our values to enable Managers to lead teams at GitLab. To date, there has not been a formal GitLab Manager training program and this will serve as our first iteration. 

**How will it help**

Learn the basic principles of what it means to be a manager using a whole person approach to leadership. The program was built to incorporate themes identified during interviews with team members and managers at GitLab. 

**What do I need to do**

Set aside time each day to participate in the challenge (20 minutes) and live learning sessions (1 hour). Complete the weekly self-evaluations and comment in the Slack channel during discussions. As a participant in the pilot program, your feedback is critical for our success so please make time to complete the weekly evaluations. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gSMrv9CiqOk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Pilot Program 

- 12 Daily Challenges (20 minutes)
- 4 Live Learning Sessions (1 hour)
- 4 Weekly Self Evaluations and Reflections (10 minutes)
- Certification Upon Completion

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BIAbuYPMd24" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Week 1

During week one we will discuss **Getting to Know your Team & Managing Underperformance**. Information covered in week one can be found on the [SOCIAL STYLES](/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/) and the [Underperformance page](/handbook/underperformance/). 

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 1    | 4 Week Challenge Intro | 20 minutes | Self-paced Daily Challenge | * Challenge Kickoff Message |  |
| Day 2    | Get to know your SOCIAL STYLE | 20 minutes | Self-paced Daily Challenge | *Read the Understanding SOCIAL STYLEs page and Complete the assessment | * Collaboration (Getting to know individuals on a personal and professional level) <br> * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |
| Day 3    | Get to know your team | 20 minutes | Self-paced Daily Challenge | * Ensure your team fills out the SOCIAL STYLEs assessment. <br> * Read the Underperformance page | * Collaboration (Getting to know individuals on a personal and professional level) <br> * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |
| Day 4    | Attend Live Learning | 50 minutes | Live Learning | * Participate in the live learning and be prepared with your Social Style! | * Results (Measure results not hours) <br> * Efficiency (Be respectful of other's time <br> * Manager of One <br> * Handbook First <br> * Iteration  |
| Day 5    | Self Reflection and Weekly Evaluation | 20 minutes | Virtual Team Meeting | * Complete the weekly self-reflection and Week 1 Evaluation | * Collaboration (Getting to know individuals on a personal and professional level) <br> * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |


### Week 2

During week two we will discuss **Trust and Coaching**. Information covered in week two can be found on the [Coaching](/handbook/people-group/learning-and-development/career-development/coaching/) and [Building Trust](/handbook/people-group/learning-and-development/building-trust) page

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 6   | Evaluting Your Team on Trust | 20 minutes | Self-paced Daily Challenge | * Read the Coaching & Trust Handbook Page <br> * Complete the survey to evaluate your team on trust | *Results <br> * Efficiency <br> * Manager of One <br> *Handbook First <br> Iteration |
| Day 7   | Introducing the Trust Equation | 20 minutes | Self-paced Daily Challenge | * Read about the Trust Equation <br> *Answer the challenge prompt | *Results <br> *Efficiency (Be respectful of other's time) <br> Manager of One <br> Handbook First <br> Iteration |
| Day 8   | What is Coaching? | 20 minutes | Self-paced Daily Challenge | * Review coaching material <br> * Share an experience when you've been coached | *DIB <br> *Collaboration <br> *Iteration |
| Day 9   | Attend Live Learning | 50 minutes | Live Learning | * Attend the coaching live learning session and be prepared to participate! | *Results (measure results not hours) <br> *Efficiency (Be respectful of other's time) <br> *Manager of One <br> *Iteration |
| Day 10  | Self Reflection and Weekly Evaluation | 20 minutes | Self-paced Daily Challenge | * Complete the Self-reflection and weekly evaluation | * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |


### Week 3

During week three we will discuss **Building and Inclusive/Belonging Environment**. Information covered in week three can be found on the [Psychological Safety](/handbook/people-group/learning-and-development/emotional-intelligence/psychological-safety) and [Diversity, Inclusion, and Belonging](/company/culture/inclusion) page. 

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 11   | Psychological Safety | 20 minutes | Self-paced Daily Challenge | * Read the Psychological Safety Handbook Page <br> * Answer the discussion question | * Diversty, Inclusion, & Belonging <br> * Emotional Intelligence |
| Day 12   | Cultures | 20 minutes | Self-paced Daily Challenge | *  Use the Hofstede Country Comparison Tool <br> * Share in the issue a screenshot of the chart with all countries represented | * Diversty, Inclusion, & Belonging |
| Day 13   | Building an Inclusive/Belonging Culture | 20 minutes | Self-paced Daily Challenge | *  Complete the Backpack Activity <br> * Share in the issue what it would be like for other members of your team to complete the backpack activity | * Diversty, Inclusion, & Belonging |
| Day 14   | Attend Live Learning | 50 minutes | Live Learning | * Attend the coaching live learning session and be prepared to participate! | * Diversty, Inclusion, & Belonging |
| Day 15   | Self Reflection and Weekly Evaluation | 20 minutes | Self-paced Daily Challenge | * Complete the Self-reflection and weekly evaluation | * Diversty, Inclusion, & Belonging |


### Week 4

During week four we will discuss **Developing High Performing Teams**. Information covered in week four can be found on the [Building High Performing Teams page](/handbook/people-group/learning-and-development/building-high-performing-teams/).  

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 16   | What is a High Performing Team | 20 minutes | Self-paced Daily Challenge | * Learn about what makes a high performing team | * Collaboration <br> * Managers of One <br> * Building High Performing Teams <br> * Emotional Intelligence |
| Day 17   | Path to High Performing Teams | 20 minutes | Self-paced Daily Challenge | * Learn about the high performance model | * Building High Performing Teams <br> * Collaboration <br> * Managers of one <br> * Handbook First |
| Day 18   | Applying the Model to Managing at GitLab | 20 minutes | Self-paced Daily Challenge | * Apply the Model to your team at GitLab | * Building High Performing Teams <br> * Collaboration <br> * Managers of One |
| Day 19   | Attend Live Learning | 50 minutes | Live Learning | * Attend the Live Learning session | * Building High Performing Teams |
| Day 20   | Self Reflection and Weekly Evaluation | 20 minutes | Self-paced Daily Challenge | * Complete the Self-Reflection | * Efficiency <br> * Results |


## FAQs

1. **What is the Manager Challenge Program?**
     - The program is intended to build a set of baseline management skills through micro habits and daily practices over the course of 4 weeks. It it a blended learning approach that combines self-paced and live learning sessions. It is intended to build a set of foundational/enhance leadership and management practices. 
1. **Is this a Pilot launch?**
     - Yes. In September we are running a pilot and will be rolling out the program to the wider GitLab team very soon.
1. **Do I have to participate?**
     - Participation is not required, but we strongly recommend that all managers invited to the pilot complete the daily challenges and actively participate. 
1. **Have we done this program in the past?**
    - This is the first time we have implemented a formal management/leadership program at GitLab. Our Managers serve a critical role in the organization and this pilot program will be the foundation for future programs.
1. **What is required of me?**
     - Monday to Wednesday there will be a series of daily challenges that a manager can complete asynchronously on their own time that will cover a range of topics. On Thursday's we will come together as a group in a live learning session to apply the challenges to real scenarios managers face in a remote environment. 
1. **I have a lot of management experience, will this help?**
     - No matter your level or tenure as a manager, we can all take time out of our busy days to grow our management and leadership skills. The more practice the better and we hope that you will find the challenges applicable to your job.
1. **What skills will I be building?**
     - You will be building a range of skills that include: coaching, managing underperformance, crucial conversations, feedback, building an inclusive culture, developing high performing teams, getting to know your team and much more!  
1. **How does this complement our values?**
     - All of the challenges will reinforce our values by applying management techniques to lead others in a remote environment. In the weekly charts above, each day or topic will have a [values competency](/handbook/competencies/#values-competencies) that you can expect to improve upon. 
1. **Is the content in the slides in the Handbook?**
     - Yes! All of the content will be in the Handbook. We are creating Google Slides to visualize the content for the challenges but all of it will live in the handbook. 
1. **What if I can’t attend a Live Learning session or complete a challenge?**
     - If you miss a challenge or live learning session you can go back and complete the challenge anytime. Each SSOT page for material covered will be linked on this page. The live learning recordings will be on the respective SSOT page (ex. the recording for the Week 3 Trust & Coaching session will be on the [Coaching page](/handbook/people-group/learning-and-development/career-development/coaching/)). 
1. **What if I am out of office for part of the program?**
     - As long as you complete the challenge and let the Learning and Development know when you have completed them, it is okay to be out of office. 
1. **I'm not a Manager, can I still participate?**
     - Yes, you can still participate but we ask that you complete the activities asynchronously for your own professional development. Once you are ready to become a manager, you will be equipped with a set of baseline management skills that will serve as a great foundation for future growth. 
1. **How will participation be tracked?**
     - Each challenge will be in the form of an Issue. The details of the challenge will be laid out in the issue. We ask that participants complete the challenge asynchronously and comment on the issue according to the challenge prompt. 
1. **Will I receive a certification?**
     - Yes! Once you complete all challenges, attend the live learning sessions, and complete the self-reflection activities, you will receive the "GitLab Managers Challenge" certificate.

## Manager Challenge Certification

Following the completion of the Gitlab Manager Challenge program, participants will be [awarded a certification](/handbook/people-group/learning-and-development/certifications/)! The certification will be sent to a team member who has successfully completed at least 80% of the daily challenges. Once a team member receives a certification, they can post it on their LinkedIn Profile. 

## Learning and Development Team 

### Organizing Epics and Issues 

The Manager Challenge Pilot is set up within the GitLab Tool. We are using Epics, Sub Epics and Issues. There is one main Epic for the whole program, a Sub Epic for each week, and then an Issue for each day. 

**Example of Manager Challenge Pilot Set Up:**

[Manager Challenge Pilot - Epic](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1)

[Week 1 - Manager Challenge Pilot - Sub Epic](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/2)

[Manager Challenge Project](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge)

[Manager Challenge Program Pilot: Day 1 - Issue](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge/-/issues/1)

**Learning & Development Team Tasks** 

- A Week before the program starts: 
   - Create the Epic: Manager Challenge - [Month] [Year] 
   - Create the Sub Epics for each week: Manager Challenge - Week 1 
- Daily
   - Create the Day's Issue in the [Manager Challenge Project](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge) using the [manager-challenge-daily-template](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge/-/blob/master/.gitlab/issue_templates/manager-challenge-daily-template.md) 
   - Monitor the issue comments to respond to questions as well as keep track of participation 

### Communication 

The modes of communciation during the challenge include Slack and GitLab. Ensure that there is a specific channel in Slack set up for challenge memebers, as well as a group tag in GitLab. 

Each day, once the issue is opened, add the group tag to the issue so participants recieve an email notification that the daily issue is ready. For the pilot, the tag was `@gl-manager-challenge-pilot`. 

The Slack channel for the pilot program was `#manager-challenge-pilot`. Each day, once the issue was opened, a message was posted in the slack channel with a link to the day's issue. 

### Scheduling 

**Live Learning Sessions** 

Each week of the challenge has one day of live learning sessions. Depending on the locations of the participants, it can be determeined how many sessions are held on the Thursday of each week.  

Approximate times for the live sessions to ensure timezone coverate include: 10:30am ET, 4pm ET, and 9:30pm ET. 

**Retrospective Sessions**

After the 20 day challenge has been completed, it is suggested to have a live retrospective where participants can come and share any feedback about the program. The time(s) this is scheduled for can be the same as the live learning sessions were during the challenge. 

For the pilot Manager Challenge program, the Learning & Development team held a [retrospective session](https://docs.google.com/document/d/1ecuYFRA2oMqpXbGeaK-VIB8AeX98dJJ2-W-wcYUzaXg/edit?userstoinvite=nadia%40gitlab.com&ts=5f7eb88b) to discuss with participants how we can iterate and improve the program moving forward. [Anonymous feedback and ratings](https://docs.google.com/spreadsheets/d/1iUqcs-kSmXf1ol_lio7TJYcn7VHEGHrNcacW3KQwyKg/edit#gid=314035882) were captured throughout the pilot challenge. Each week participants filled out a learning evaluation form for the weekly challenges and overall program.  

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/v_ohUbaRFFw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
</figure>
