---
layout: markdown_page
title: "Category Direction - Code Testing and Coverage"
canonical_path: "/direction/verify/code_testing/"
---

- TOC
{:toc}

## Code Testing

Code testing and coverage ensure that individual components built within a pipeline perform as expected, and are an important part of a Continuous Integration framework. Our vision for this category is to make the feedback loop for developers as short as possible, eventually enabling users to go from first commit to code in production in only an hour with confidence.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Testing%20and%20Coverage)
- [Overall Vision](/direction/ops/#verify)

Interested in joining the conversation for this category? Please join us in the issues where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why
Users are excited to have data and a visual indicator of the direction code coverage is trending in a project. For users that have dozens to thousands of projects though having an even higher level view of coverage is often required, especially in the enterprise. Providing a [data download of all coverage data in a group](https://gitlab.com/gitlab-org/gitlab/-/issues/215104) to reduce manual work was a good first step.

The next steps are to display select data in GitLab for [selected projects](https://gitlab.com/gitlab-org/gitlab/-/issues/215135) and a [graph of the average coverage](https://gitlab.com/gitlab-org/gitlab/-/issues/215140) over all projects which will close out the work for [Code Coverage for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838).

## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)). Key deliverables to achieve this are included in these epics:

* [Code Coverage Data for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838)
* [Historic Test Data for projects](https://gitlab.com/groups/gitlab-org/-/epics/3129)
* [Unit Test Report Usability enhancements](https://gitlab.com/groups/gitlab-org/-/epics/2950)

We may find in research that only some of the issues in these epics are needed to move the vision for this category maturity forward. The work to move the maturity is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/3660).

## Competitive Landscape

Many other CI solutions can also consume standard JUnit test output or other formats to display insights natively like [CircleCI](https://circleci.com/docs/2.0/collect-test-data/) or through a plugin like [Jenkins](https://plugins.jenkins.io/junit). 

There are new entries in the code testing space utilizing ML/AI tech to optimize test execution like [Launchable](https://launchableinc.com/solution/) and even write test cases like [Diffblue](https://www.diffblue.com/).

In order to stay remain ahead of these competitors we will continue to push forward to make unit test data visible and actionable in the context of the Merge Request for developers with [unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html#viewing-unit-test-reports-on-gitlab) and historical insights to identify flaky tests with issues like [gitlab#33932](https://gitlab.com/gitlab-org/gitlab/issues/33932).

## Top Customer Success/Sales Issue(s)

Sales has requested a higher level view of testing and coverage data for both projects and groups from the Testing Group. A first step towards this will be the display of [coverage data for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838).

## Top Customer Issue(s)

The most popular issue(s) in the Code Testing and Coverage category today are requests to be able to enforce [a code coverage increase or hold](https://gitlab.com/gitlab-org/gitlab/-/issues/15765) or [a coverage percentage](https://gitlab.com/gitlab-org/gitlab/-/issues/6284) before a merge. 

Another popular issue is a request to see the [code coverage badge on any branch](https://gitlab.com/gitlab-org/gitlab/-/issues/27093) which would solve common problem for users of long lived branches who do not have a view of the test coverage of those branches today.

## Top Internal Customer Issue(s)

The GitLab Quality team has opened an interesting issue, [Provide API to retrieve test case durations from a pipeline](https://gitlab.com/gitlab-org/gitlab/issues/14954), that is aimed at solving a problem where they have limited visibility into long test run times that can impact efficiency.

## Top Vision Item(s)
The top vision item is [Detect and report on flaky tests](https://gitlab.com/gitlab-org/gitlab/issues/3673) which will start to address the problem of flaky test results which cause developers to not trust test runs or force unnecessary reruns of tests. Both of those outcomes are undesirable and counter to our goal of minimizing the lead time of changes. The Testing team had a [good discussion](https://www.youtube.com/watch?v=i3k5-EC0KYM&feature=youtu.be) about this as part of a Think Big session in July, 2020. The next step towards this are implementation of test history on the [Test Summary Widget](https://gitlab.com/gitlab-org/gitlab/-/issues/241759) and [Unit Test Report](https://gitlab.com/gitlab-org/gitlab/-/issues/235525) to learn if there is value in tracking and displaying test failures.

We are also looking to provide a one stop place for CI/CD leaders with [Director-level CI/CD dashboards](https://gitlab.com/gitlab-org/gitlab/issues/199739). Quality is an important driver for improving our users ability to confidently track deployments with GitLab and so we are working next on a view of code coverage data over time for across a [group's projects](https://gitlab.com/groups/gitlab-org/-/epics/2838).

We have started brainstorming some ideas for the vision and captured that as a rough design idea you can see below.

![Design for Vision of Code Testing and Coverage data summary](/images/code-testing-data-view-vision.png)

