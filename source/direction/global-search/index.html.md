---
layout: markdown_page
title: "Category Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

## Global Search

| | |
| --- | --- |
| Section | [Enablement](/direction/enablement/) |
| Content Last Reviewed | `2020-07-24` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting this direction page on Global Search in GitLab. This page belongs to the [Global Search](/handbook/product/product-categories/#global-search-group) group of the Enablement stage and is maintained by John McGuire([E-Mail](mailto:jmcguire@gitlab.com)).

This strategy evolves, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGlobal%20Search) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGlobal%20Search) on this page. Sharing your feedback directly on GitLab.com or submitting a Merge Request to this page are the best ways to contribute to our direction.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for search, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is. If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it. Please include use cases, personas, and user journeys into this section. -->

GitLab currently supports [Advanced Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html) for Starter and above self-managed instances. This provides users with a faster and more complete search experience across GitLab. GitLab.com similarly offers Advanced Search for Bronze and above.

We chronicled our journey of deploying Elasticsearch for GitLab.com through several blog posts.
* [2019-03-20 Lessons from our journey to enable global code search with Elasticsearch on GitLab.com](/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [2019-07-16 Update: The challenge of enabling Elasticsearch on GitLab.com](/releases/2019/07/16/elasticsearch-update/)
* [2020-04-28 Update: Elasticsearch lessons learnt for Advanced Search](/blog/2020/04/28/elasticsearch-update/)

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.-->

Advanced Search is targeted at all personas who might use GitLab. However, the largest benefits come to users performing cross-project code search looking for innersourcing opportunities or exploring the breadth of public projects across GitLab.com.

#### Challenges to address

GitLab is gorwing and the path to delivering a world class Git Repo search is evolves quickly. Enterprise Edition Self-Managed customers need to provide there own install of Elasticsearch to conenct to GitLab.
We will need to be very creative about how to advace Community Edition and GitLab.com Free. Contributions are welcome!

### Maturity

Currently, GitLab's maturity for Search is _viable_. Here's why:

 - GitLab's current Advanced Search experience works for some self-managed instances and the experience of getting started has continued to improve.

The UI lacks some basic search capabilites. Using the search generally requires the user to be very exact in what they are looking for. It offers very little in exploring what might be avilable.


### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top priorities for a few stakeholders. This section must provide a link to an issue or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

We have added a live roadmap that will track the progess as we complete major epics.
[Global Search Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aglobal%20search&label_name[]=Roadmap)

**In Progress: [Advanced Search Operational maturity on GitLab.com and EE](https://gitlab.com/groups/gitlab-org/-/epics/2132)** - Now that Advanced Search has been enabled for GitLab.com, we are focusing on support our [dogfooding](/handbook/values/#dogfooding) principles as we make the feature less intensive to operate.

**Next: [UX Search Enhancements for Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/1505)** - Our current User Interface was designed to provide minimal amount of complexity as we focused on building and scaling the backend componets.  Now we can start adding more rich features that are avilable with Elasticsearch. I expect that once we have these features avilable we will do another search UX design that better utilizies these features with a more complete User Flow.

### What is Not Planned right now

Currently there is not a plan to scale beyond the needs of Paid Groups on GitLab.com. This means that while the ambition of the Search Group is to expand Advanced Search to all users of GitLab, we're not yet ready to move in that direction.

The search group is also not focused on expanding the use of Advanced Search to other search areas beyond Global Search. While areas like Issue and Epic search could benefit from some of the features available, those areas are not in scope at this time.

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk about, but we’re talking with customers about what they actually use, and ultimately what they need.-->

Both GitHub and BitBucket provide a more comprehensive and complete search for users; particularly in their ability to deeply search code and surface those results to users. While GitLab's Advanced Search is available to self-managed users.

<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most thumbs-up), but you may have a different item coming out of customer calls.-->

- [UX Search Enhancements for Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/1505)

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

- [Advanced Search Operational maturity on GitLab.com and EE](https://gitlab.com/groups/gitlab-org/-/epics/2132)

### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Advanced Search Ranking](https://gitlab.com/groups/gitlab-org/-/epics/3729)
